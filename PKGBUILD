# Maintainer: Daniele Oriani <daniele.oriani.gm at gmail.com>

pkgname=tabbed-git
_pkgname=tabbed
pkgver=20180311.b5f9ec6
pkgrel=1
pkgdesc='Simple generic tabbed fronted to xembed aware applications, originally designed for surf but also usable with many other applications'
url='http://tools.suckless.org/tabbed/'
arch=('i686' 'x86_64')
license=('MIT')
depends=('libx11')
makedepends=('git')
patches=('http://tools.suckless.org/tabbed/patches/clientnumber/tabbed-clientnumber-20160702-bc23614.diff' \
         'tabbed-bindings.patch')
source=("git://git.suckless.org/tabbed" \
        "${patches[@]}")
sha1sums=('SKIP'
          '3b6580970a2030f715e04a14dc1f05ed2bcd344f'
          '897ed6279cd012027d2593e6a43665feb28eb49e')

provides=("${_pkgname}")
conflicts=("${_pkgname}")

pkgver() {
  cd "${srcdir}/${_pkgname}"
  git log -1 --format='%cd.%h' --date=short | tr -d -
}

prepare() {
  cd "${srcdir}/${_pkgname}"
  local patchfile='tabbed-clientnumber-20160702-bc23614.diff'
  echo "Applying patch" ${patchfile}"..."
  patch -Np1 < "${srcdir}/${patchfile}"
  patchfile='tabbed-bindings.patch'
  echo "Applying patch" ${patchfile}"..."
  patch -Np1 < "${srcdir}/${patchfile}"
  sed -r\
          -e '/char font/s/= .*/= "Iosevka Term Slab:size=12";/' \
          -e '/normbgcolor/s/= .*/= "#745940";/' \
          -e '/selbgcolor/s/= .*/= "#745940";/' \
          -e '/normfgcolor/s/= .*/= "#302921";/' \
          -e '/selfgcolor/s/= .*/= "#FFF2E7";/' \
          -e '/int +newposition/s/= .*/= 1;/' \
          -e '/Bool npisrelative/s/= .*/= True;/' \
          -i config.def.h
  sed \
          -e 's/CPPFLAGS =/CPPFLAGS +=/g' \
          -e 's/CFLAGS =/CFLAGS +=/g' \
          -e 's/LDFLAGS =/LDFLAGS +=/g' \
          -e 's/_BSD_SOURCE/_DEFAULT_SOURCE/' \
          -i config.mk
  sed '/@tic/d' -i Makefile
}

build() {
	cd "${srcdir}/${_pkgname}"
	make X11INC=/usr/include/X11 X11LIB=/usr/lib/X11
}

package() {
	cd "${srcdir}/${_pkgname}"
	make PREFIX=/usr DESTDIR="${pkgdir}" install
	install -Dm644 LICENSE "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
	install -Dm644 README "${pkgdir}/usr/share/doc/${pkgname}/README"
}
